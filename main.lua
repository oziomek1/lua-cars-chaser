-- Wojciech Ozimek

function love.load()
    love.window.setTitle('Cars chaser')
    speedLimit = {['min'] = 50, ['max'] = 300}
    steerAngleLimit = 0.5
    speedStep = 200
    steerAngleStep = 2
    frictionFactor = 0.9
    windowSize = {['width'] = 800, ['height'] = 600}
    border = 30
    tile = love.graphics.newImage("Road.png")
    tileWidth = tile:getWidth()
    tileHeight = tile:getHeight()
    tileMap = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    }
    secondsUntilNewChaser = 3

    runner = create_sprite(400, 300, "BlueStrip.png")
    runner.wheelBase = 30

    chaser = create_sprite(200, 200)
    listOfChasers = {}
    table.insert(listOfChasers, chaser)
    startTime = love.timer.getTime()
end

function create_sprite(x, y, image_path, grip)
    image_path = image_path or "RedStrip.png"
    grip = grip or frictionFactor
    sprite = {x = x, y = y, image = love.graphics.newImage(image_path), grip = grip, speed = 50, heading = 0, steerAngle = 0}
    sprite.origin_x = sprite.image:getWidth() / 2
    sprite.origin_y = sprite.image:getHeight() / 2
    return sprite;
end

function clip_value(value, lower, upper)
    if lower > upper then 
        lower, upper = upper, lower
    end
    return math.max(lower, math.min(upper, value))
end

function update_chaser_position(angle, speed, positionX, positionY, dt, frictionFactor)
    cos = math.cos(angle)
    sin = math.sin(angle)

    positionX = positionX + speed * cos * dt * frictionFactor
    positionY = positionY + speed * sin * dt * frictionFactor
    return positionX, positionY;
end

function get_distance(x1, y1, x2, y2) 
    return math.sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2);
end

function calculate_chaser_speed(speed, distance, speedLimit, frictionFactor)
    if speed <= speedLimit['max'] and speed >= speedLimit['min'] then
        speed = speed * 0.5 * (distance / speedLimit['min'])
    end
    return clip_value(speed, speedLimit['min'], speedLimit['max']);
end

function love.keypressed(key)
    if key == "space" then
        chaser = create_sprite(math.random(50, 750), math.random(50, 550), "RedStrip.png", math.random())
        table.insert(listOfChasers, chaser)
    end
end

function love.update(dt)
    runner.frontWheelx, runner.frontWheely, runner.rearWheelx, runner.rearWheely = calculate_runner_wheels_position(
        runner.x, runner.y, runner.speed, runner.wheelBase, runner.heading, runner.steerAngle, dt)

    runner.x = (runner.frontWheelx + runner.rearWheelx) / 2
    runner.y = (runner.frontWheely + runner.rearWheely) / 2

    if runner.x < border then
        runner.x = border
    elseif runner.x > windowSize['width'] - border then
        runner.x = windowSize['width'] - border
    end

    if runner.y < border then
        runner.y = border 
    elseif runner.y > windowSize['height'] - border then
        runner.y = windowSize['height'] - border
    end

    runner.heading = math.atan2(runner.frontWheely - runner.rearWheely, runner.frontWheelx - runner.rearWheelx)
    runner.speed = calcualate_runner_speed(runner.speed, speedLimit, speedStep, frictionFactor, dt)
    runner.steerAngle = calculate_runner_steering_angle(runner.steerAngle, steerAngleLimit, steerAngleStep, frictionFactor, dt)

    for i, chaser in ipairs(listOfChasers) do
        chaser.angle = math.atan2(runner.y - chaser.y, runner.x - chaser.x)
        chaser.x, chaser.y = update_chaser_position(chaser.angle, chaser.speed, chaser.x, chaser.y, dt, frictionFactor)
        chaser.speed = calculate_chaser_speed(chaser.speed, get_distance(chaser.x, chaser.y, runner.x, runner.y), speedLimit, frictionFactor)
    end

    runner.prev_x = runner.x
    runner.prev_y = runner.y
    if checkCollision(runner, listOfChasers) then
        love.load()
    end

    result = love.timer.getTime() - startTime
    if result > secondsUntilNewChaser then
        chaser = create_sprite(math.random(50, 750), math.random(50, 550), "RedStrip.png", math.random())
        table.insert(listOfChasers, chaser)
        startTime = love.timer.getTime()
    end
end

function love.draw(dt)
    for i=1, #tileMap do
        for j=1, #tileMap[i] do
            if tileMap[i][j] == 0 then
                love.graphics.draw(tile, (j - 1) * tileWidth, (i - 1) * tileHeight)
            end
        end
    end

    for i, chaser in ipairs(listOfChasers) do
        love.graphics.draw(chaser.image,
            chaser.x, chaser.y, chaser.angle, 1, 1,
            chaser.origin_x, chaser.origin_y)
    end

    love.graphics.draw(runner.image,
        runner.x, runner.y, runner.heading, 1, 1,
        runner.origin_x, runner.origin_y)

    love.graphics.circle("fill", runner.x, runner.y, 5)
end

function calcualate_runner_speed(speed, speedLimit, speedStep, frictionFactor, dt)
    if love.keyboard.isDown("down") and speed > -speedLimit['min'] then
        speed = speed - speedStep * dt
    elseif love.keyboard.isDown("up") and speed < speedLimit['max'] then
        speed = runner.speed + speedStep * dt
    else
        if speed > 0 then
            speed = speed - math.log(speed + 1)
        else
            speed = speed * frictionFactor
        end
    end
    return speed;
end

function calculate_runner_steering_angle(steerAngle, steerAngleLimit, steerAngleStep, frictionFactor, dt)
    if love.keyboard.isDown("left") and math.abs(steerAngle) < steerAngleLimit then
        steerAngle = steerAngle - steerAngleStep * dt
    elseif love.keyboard.isDown("right") and math.abs(steerAngle) < steerAngleLimit then
        steerAngle = steerAngle + steerAngleStep * dt
    else
        steerAngle = steerAngle * frictionFactor
    end

    return steerAngle;
end

function calculate_runner_wheels_position(positionX, positionY, speed, wheelBase, heading, steerAngle, dt)
    frontWheelx = positionX + wheelBase / 2 * (math.cos(heading))
    frontWheely = positionY + wheelBase / 2 * (math.sin(heading))

    rearWheelx = positionX - wheelBase / 2 * (math.cos(heading))
    rearWheely = positionY - wheelBase / 2 * (math.sin(heading))

    rearWheelx = rearWheelx + speed * dt * math.cos(heading)
    rearWheely = rearWheely + speed * dt * math.sin(heading)

    frontWheelx = frontWheelx + speed * dt * math.cos(heading + steerAngle)
    frontWheely = frontWheely + speed * dt * math.sin(heading + steerAngle)
    return frontWheelx, frontWheely, rearWheelx, rearWheely;
end

function checkCollision(runner, listOfChasers)
    for i, chaser in ipairs(listOfChasers) do
        if get_distance(chaser.x, chaser.y, runner.x, runner.y) < 40 then
            return true
        end
    end
    return false
end