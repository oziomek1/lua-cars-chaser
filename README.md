## Lua 
Lua script with love2d frameworks

### To run project visit:
[https://love2d.org/wiki/Getting_Started](https://love2d.org/wiki/Getting_Started)

### On MacOS

/Applications/love.app/Contents/MacOS/love ~/path/to/project_directory

## Game

#### You're goal is to run away from enemies. 
You are driving a blue car. Opponents are in red cars
Every 3 seconds new opponent appears in a random place

#### Can drive a car using arrows:
* Up: forward
* Down: break / reverse
* Left: steering left
* Right: steering right


<img src="cars-chaser.png" width="100%" />

